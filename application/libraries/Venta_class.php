<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Venta_class extends Arnold_class {

    public $venta_id;
    public $agente_agente_id;
    public $venta_fecha;
    public $venta_sillas;
    public $venta_total;
    protected $agente;

}
