<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Agente_class extends Arnold_class {

    public $agente_id;
    public $agente_nombre;
    public $agente_apellidos;
    protected $ventas;

    /**
     * Retorna nombre completo del agente
     * @return string
     */
    public function get_nombre_completo() {
        return $this->agente_nombre . " " . $this->agente_apellidos;
    }

    public function get_total_ventas() {
        $total = 0;
        foreach ($this->ventas as $v) {
            $total += $v->venta_total;
        }
        return $total;
    }

}
