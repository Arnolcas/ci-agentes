<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Arnold_class {

    public function __construct($params = array()) {
        
    }

    public function __get($attr) {
        if (isset(get_instance()->$attr)) {
            return get_instance()->$attr;
        }
        if (property_exists($this, $attr)) {
            return $this->$attr;
        }
        // var_dump($this);
        echo 'Error<br>Propiedad "' . $attr . '"no Existe: ';
        exit();
    }

    public function __set($name, $value) {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }
    }

    public function __isset($name) {
        return isset($this->$name);
    }

}
