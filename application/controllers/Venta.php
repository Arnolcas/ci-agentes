<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Venta extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Venta_model');
        $this->load->model('Agente_model');
        $this->load->helper('agente_helper');
    }

    public function lista() {
        $ventas = $this->Venta_model->retornar_ventas();
        $html = $this->load->view('venta/sidebar', [], true);
        $html .= $this->load->view('venta/lista', ['ventas' => $ventas], true);

        $this->load->view('template/head');
        $this->load->view('template/content', ['html' => $html]);
        $this->load->view('template/footer');
    }

    public function crear() {


        $html = $this->load->view('venta/crear', [], true);

        $this->load->view('template/head');
        $this->load->view('template/content', ['html' => $html]);
        $this->load->view('template/footer');
    }

    public function insertar() {
        $post = $this->input->post();
        $fecha = $post['fecha'];
        $agente_id = $post['agente'];
        $sillas = $post['sillas'];
        $total = $post['total'];

        $venta = new Venta_class();
        $venta->agente_agente_id = $agente_id;
        $venta->venta_fecha = $fecha;
        $venta->venta_sillas = $sillas;
        $venta->venta_total = $total;

        if ($this->Venta_model->insertar_venta($venta)) {
            redirect(base_url('venta/lista'));
        } else
            show_404();
    }

}
