<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->load->model("Venta_model");
        $total = $this->Venta_model->retornar_total_sillas();
        $html = <<<HTML
      <div class="col-12">
        <h1>Total Sillas Vendidas: $total</h1>
    </div>           
HTML;

        $this->load->view('template/head');
        $this->load->view('template/content', ['html' => $html]);
        $this->load->view('template/footer');
    }

}
