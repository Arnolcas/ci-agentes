<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Agente extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Agente_model');
    }

    public function crear() {
        $html = $this->load->view('agente/sidebar', [], true);
        $html .= $this->load->view('agente/crear', [], true);

        $this->load->view('template/head');
        $this->load->view('template/content', ['html' => $html]);
        $this->load->view('template/footer');
    }

    public function insertar() {
        $post = $this->input->post();
        $nombre = $post['nombre'];
        $apellidos = $post['apellidos'];

        $agente = new Agente_class();
        $agente->agente_nombre = $nombre;
        $agente->agente_apellidos = $apellidos;

        if ($this->Agente_model->insertar_agente($agente) != 0) {
            redirect(base_url('agente/mostrar/' . $agente->agente_id));
        } else
            show_404();
    }

    public function mostrar($agente_id) {

        $agente = $this->Agente_model->retornar_agente($agente_id);
        if ($agente != NULL) {
            $html = $this->load->view('agente/sidebar', [], true);
            $html .= $this->load->view('agente/agente', ['agente' => $agente], true);
            $html .= $this->load->view('agente/lista_ventas', ['ventas' => $agente->ventas], true);

            $this->load->view('template/head');
            $this->load->view('template/content', ['html' => $html]);
            $this->load->view('template/footer');
        } else
            show_404();
    }

    public function actualizar() {
        $post = $this->input->post();
        $nombre = $post['nombre'];
        $apellidos = $post['apellidos'];
        $agente_id = $post['id'];

        $agente = $this->Agente_model->retornar_agente($agente_id);
        $agente->agente_nombre = $nombre;
        $agente->agente_apellidos = $apellidos;

        if ($this->Agente_model->actualizar_agente($agente)) {
            redirect(base_url('agente/mostrar/' . $agente->agente_id));
        } else
            show_404();
    }

    public function lista() {
        $agentes = $this->Agente_model->retornar_agentes();
        $html = $this->load->view('agente/sidebar', [], true);
        $html .= $this->load->view('agente/lista', ['agentes' => $agentes], true);

        $this->load->view('template/head');
        $this->load->view('template/content', ['html' => $html]);
        $this->load->view('template/footer');
    }

    public function borrar($agente_id) {
        if ($this->Agente_model->borrar_agente($agente_id)) {
            redirect(base_url('agente/lista/'));
        } else
            show_404();
    }

}
