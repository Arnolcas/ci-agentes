<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



if (isset($select)) {
    $opciones = "";
    foreach ($select['datos'] as $valor => $titulo) {
        $selected = "";
        if ($select['selected'] == $valor)
            $selected = "selected";
        $opciones .= "<option value=\"" . $valor . "\" " . $selected . ">" . $titulo . "</option>";
    }
}
?>

<select class="form-control <?= (isset($select['html_class']) ? $select['html_class'] : "") ?>" id="<?= $select['html_id'] ?>" name="<?= $select['name'] ?>" <?= $select['props'] ?>>
    <?= $opciones ?>
</select>