<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>



<div class="col-xl-8 col-lg-8 order-lg-1 mb-3">
    <div class="col-12">
        <h1>Agentes</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered table-centered mb-0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Apellidos</th>
                            <th>Nombre</th>
                            <th>Total Venta</th>
                            <th class="text-center">...</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($agentes)) {
                            foreach ($agentes as $agente) {
                                ?>
                                <tr>
                                    <td class="table-user">
                                        <?= $agente->agente_id ?>
                                    </td>
                                    <td><?= $agente->agente_apellidos ?></td>
                                    <td><?= $agente->agente_nombre ?></td>
                                    <td class="table-action text-center">
                                        <a href="<?= base_url('agente/mostrar/' . $agente->agente_id) ?>" class="action-icon" target="_blank"> <i class="fa fa-eye"></i>
                                        </a>
                                        &nbsp;
                                        &nbsp;
                                        <a href="<?= base_url('agente/borrar/' . $agente->agente_id) ?>" class="action-icon" target="_blank"> <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                    <td><?= $agente->get_total_ventas() ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>     
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>