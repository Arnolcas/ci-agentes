<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="col-xl-8 col-lg-8 order-lg-1 mb-3">
    <div class="col-12">
        <h1>Crear Agente</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form method="POST" action="<?= base_url("/agente/insertar") ?>">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nombre</label>
                        <input type="text" class="form-control" name="nombre" autocomplete="off" required> 
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Apellidos</label>
                        <input type="text" class="form-control" name="apellidos" autocomplete="off" required>
                    </div>
                    <div class="form-group d-flex justify-content-end" id="form_submit">
                        <!-- Submit Button -->
                        <button id="submit" class="btn btn-primary" >Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>