<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="col-xl-8 col-lg-8 order-lg-1 mb-3">
    <div class="col-12">
        <h1>Agregar Venta</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form method="POST" action="<?= base_url("/venta/insertar/") ?>">
                    <div class="form-group">
                        <label for="exampleFormControlInput2">Fecha</label>
                        <input type="text" class="form-control" name="fecha" value="<?= date('Y-m-d') ?>" placeholder="12-31-2020" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2">Agente</label>
                        <?= crear_selector_agentes() ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput3">No. Sillas</label>
                        <input type="number" class="form-control" name="sillas" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput3">Total</label>
                        <input type="number" class="form-control" name="total" autocomplete="off" required>
                    </div>
                    <div class="form-group d-flex justify-content-end" id="form_submit">
                        <!-- Submit Button -->
                        <button id="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>