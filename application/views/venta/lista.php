<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<div class="col-xl-8 col-lg-8 order-lg-1 mb-3">
    <div class="col-12">
        <h1>Ventas</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered table-centered mb-0">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Agente</th>
                            <th>No Sillas</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total = 0;
                        if (count($ventas)) {
                            foreach ($ventas as $venta) {
                                $total += $venta->venta_total;
                                ?>
                                <tr>
                                    <td class="table-user">
                                        <?= $venta->venta_fecha ?>
                                    </td>
                                    <td><?= $venta->agente->get_nombre_completo() ?></td>
                                    <td><?= $venta->venta_sillas ?></td>
                                    <td><?= $venta->venta_total ?></td>

                                </tr>
                                <?php
                            }
                        }
                        ?>    

                        <tr>
                            <td></td>
                            <td></td>
                            <td>Total</td>
                            <td><?= $total ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>