<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists("crear_selector_agentes")) {

    function crear_selector_agentes($selected = '') {
        $data = array();
        $CI = &get_instance();
        $CI->load->helper('formulario_helper');
        $agentes = $CI->Agente_model->retornar_agentes();
        foreach ($agentes as $a) {
            $data[$a->agente_id] = $a->get_nombre_completo();
        }
        return crear_selector('agente', 'agente', $data, $selected, '', 'required', false);
    }

}