<?php

if (!function_exists("crear_selector")) {

    /**
     * 
     * @param type $name
     * @param type $html_id
     * @param type $datos
     * @param type $selected
     * @param type $html_class
     * @param type $props
     * @param type $can_be_empty
     * @return type
     */
    function crear_selector($name, $html_id, $datos, $selected = '', $html_class = '', $props = '', $can_be_empty = true) {
        if ($can_be_empty) {
            $datos_tmp = array('' => "");
            foreach ($datos as $k => $v) {
                $datos_tmp[$k] = $v;
            }
            $datos = $datos_tmp;
        }
        // var_dump($datos);
        // exit();

        $CI = &get_instance();
        $html = $CI->load->view('framework/formulario/select', array('select' => array('name' => $name, 'html_id' => $html_id, 'datos' => $datos, 'selected' => $selected, 'html_class' => $html_class, 'can_be_empty' => $can_be_empty, 'props' => $props)), true);
        return $html;
    }

}