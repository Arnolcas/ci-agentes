<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Agente_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('Agente_class');
        $this->load->model('Venta_model');
    }

    /**
     * Insertar un agente en la base de datos y retorna el id generado, retorna 0 en caso de error
     * @param Agente_class $agente
     * @return int
     */
    public function insertar_agente(Agente_class $agente) {
        $this->db->insert('agente', $agente);
        $error = $this->db->error();
        if ($error['code'] == null) {
            $agente->agente_id = $this->db->insert_id();
            return $agente->agente_id;
        }

        return 0;
    }

    /**
     * Actualiza la informacion de una gente en la base de datos
     * @return boolean
     */
    public function actualizar_agente(Agente_class $agente) {
        $this->db->where("agente_estado != 'D'");
        $this->db->where('agente_id', $agente->agente_id);
        $this->db->update('agente', $agente);
        $error = $this->db->error();
        if ($error['code'] == null) {
            return true;
        }
        return false;
    }

    /**
     * Retorna un agente de la base de datos segun su id, retornar NULL en caso de error
     * @param int $agente_id
     * @return Agente_class
     */
    public function retornar_agente($agente_id) {
        $this->db->where("agente_estado != 'D'");
        $this->db->where('agente_id', $agente_id);
        $result = $this->db->get('agente');
        $error = $this->db->error();
        if ($error['code'] == null) {
            $result = $result->custom_row_object(0, "Agente_class");
            $result->ventas = $this->Venta_model->retornar_ventas_agente($result->agente_id);
            return $result;
        }
        return NULL;
    }

    /**
     * Devuelve un array con todos los angentes en la base de datos
     * @return Agente_class[]
     */
    public function retornar_agentes() {
        $this->db->where("agente_estado != 'D'");
        $result = $this->db->get('agente');
        $error = $this->db->error();
        if ($error['code'] == null) {
            $result = $result->custom_result_object("Agente_class");
            foreach($result as $r){
                $r->ventas = $this->Venta_model->retornar_ventas_agente($r->agente_id);
            }
            return $result;
        }
        return NULL;
    }

    public function borrar_agente($agente_id) {
        $this->db->where('agente_id', $agente_id);
        $this->db->set('agente_estado', 'D');
        $this->db->update('agente');
        $error = $this->db->error();
        if ($error['code'] == null) {
            return true;
        }
        return false;
    }

}
