<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Venta_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->library('Venta_class');
    }

    public function insertar_venta(Venta_class $venta) {
        $this->db->insert('venta', $venta);
        $error = $this->db->error();
        if ($error['code'] == null) {
            $venta->venta_id = $this->db->insert_id();
            return $venta->venta_id;
        }

        return 0;
    }

    public function retornar_venta($venta_id) {
        $this->db->where('venta_id', $venta_id);
        $result = $this->db->get('venta');
        $error = $this->db->error();
        if ($error['code'] == null) {
            $result = $result->custom_row_object(0, "Venta_class");
            return $result;
        }
        return NULL;
    }

    public function retornar_ventas() {
        $result = $this->db->get('venta');
        $error = $this->db->error();
        if ($error['code'] == null) {
            $result = $result->custom_result_object("Venta_class");
            foreach ($result as $r) {
                $r->agente = $this->Agente_model->retornar_agente($r->agente_agente_id);
            }
            return $result;
        }
        return NULL;
    }

    public function retornar_ventas_agente($agente_id) {
        $this->db->where('agente_agente_id', $agente_id);
        $this->db->order_by('venta_fecha', 'DESC');
        $result = $this->db->get('venta');
        $error = $this->db->error();
        if ($error['code'] == null) {
            $result = $result->custom_result_object("Venta_class");
            return $result;
        }
        return NULL;
    }

    public function retornar_total_sillas() {
        $this->db->select('sum(venta_sillas) as total_venta');
        $result = $this->db->get('venta');
        $result = $result->row_array();
        return $result['total_venta'];
    }

}
